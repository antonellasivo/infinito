<?php

use Carbon\Carbon;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('route');
            $table->timestamps();
        });

        $categories = [
            ['name'=>'A scuola d\'Infinito', 'route'=>"attività-e-corsi/a-scuola-di-infinito"],
            ['name'=>'Giocando s\'impara', 'route'=>"attività-e-corsi/giocando-si-impara"],
            ['name'=>'A scuola di Danza Meditativa', 'route'=>"attività-e-corsi/danza-meditativa"],
            ['name'=>'Danza Meditativa con i Chakra', 'route'=>"attività-e-corsi/danza-medidativa-chakra"],
            ['name'=>'Lezioni in campo aperto', 'route'=>"attività-e-corsi/campo-aperto"],
            ['name'=>'Mandala: Cherchi di Sé', 'route'=>"attività-e-corsi/mandala"],
            ['name'=>'Il Suono della Vita', 'route'=>"attività-e-corsi/suono-della-vita"],
        ];
       
        foreach($categories as $category){
            DB::table('categories')->insert([
                'name'=>$category['name'],
                'route'=>$category['route'],
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
};
