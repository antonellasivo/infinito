<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ActivityController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'welcome'])->name('welcome');

//Articoli
Route::get('/nuovo/articolo', [ArticleController::class, 'createArticle'])->middleware('auth')->name('articles.create');
Route::get('/tutti-gli-articoli', [ArticleController::class, 'indexArticle'])->name('articles.index');

//Attività e corsi
Route::get('attività-e-corsi', [ActivityController::class, 'index'])->name('activity.index');
Route::get('attività-e-corsi/a-scuola-di-infinito', [ActivityController::class, 'aScuolaDiInfinito'])->name('aScuolaDiInfinito');
Route::get('attività-e-corsi/giocando-si-impara', [ActivityController::class, 'giocandoImpara'])->name('giocandoImpara');
Route::get('attività-e-corsi/danza-meditativa', [ActivityController::class, 'danzaMeditativa'])->name('danza-meditativa');
Route::get('attività-e-corsi/danza-medidativa-chakra', [ActivityController::class, 'danzaMeditativaChakra'])->name('danza-meditativa-chakra');
Route::get('attività-e-corsi/campo-aperto', [ActivityController::class, 'campoAperto'])->name('campo-aperto');
Route::get('attività-e-corsi/mandala', [ActivityController::class, 'mandala'])->name('mandala');
Route::get('attività-e-corsi/suono-della-vita', [ActivityController::class, 'suonoDellaVita'])->name('suono-della-vita');

//Eventi
Route::get('/nuovo/evento', [EventController::class, 'createEvent'])->middleware('auth')->name('events.create');
Route::get('/tutti-gli-eventi', [EventController::class, 'indexEvent'])->name('events.index');