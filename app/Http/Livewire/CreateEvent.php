<?php

namespace App\Http\Livewire;

use App\Models\Event;
use Livewire\Component;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;

class CreateEvent extends Component
{
    public $title;
    public $description;
    public $day;
    public $month;
    public $year;
    public $category;

    protected $rules = [
        'title'=>'required|min:8',
        'description'=>'required|min:20',
        'day'=>'required|max:31|min:1|numeric',
        'month'=>'required|max:12|min:1|numeric',
        'year'=>'required|max:2028|min:2022|numeric',
        'category'=>'required',
    ];

    protected $messages = [
        'required' => 'Il campo :attribute è richiesto',
        'min' => 'Il campo :attribute è troppo corto',
        'max' => 'Il campo :attribute è troppo lungo',
        'numeric' => 'Il campo :attribute deve essere un numero',
       
    ];

    public function store(){
        $category = Category::find($this->category);
        $event = $category->events()->create([
            'title'=>$this->title,
            'description'=>$this->description,
            'day'=>$this->day,
            'month'=>$this->month,
            'year'=>$this->year,
        ]);
        Auth::user()->events()->save($event);
       
        session()->flash('message', 'Evento creato con successo');
        $this->cleanForm();
    }

    public function updated($propertyName){
        $this->validateOnly($propertyName);
    }

    public function cleanForm(){
        $this->title = '';
        $this->description = '';
        $this->day = '';
        $this->month = '';
        $this->year = '';
        $this->category = '';
    }

    public function render()
    {
        return view('livewire.create-event');
    }
}
