<?php

namespace App\Http\Livewire;

use App\Models\Article;
use Livewire\Component;

class CreateArticle extends Component
{
    public $title;
    public $description;
    public $body;

    protected $rules = [
        'title' => 'required|min:5',
        'description' => 'required|min:150',
        'body' => 'required|min:250'
    ];

    protected $messages = [
        'required' => 'Il campo :attribute è richiesto',
        'min' => 'Il campo :attribute è troppo corto',
    ];

    public function store(){
        Article::create([
            'title'=>$this->title,
            'description'=>$this->description,
            'body'=>$this->body,
        ]);
        session()->flash('message', 'Articolo inserito con successo');
        $this->cleanForm();
    }
    public function updated($propertyName){
        $this->validateOnly($propertyName);
    }

    public function cleanForm(){
        $this->title="";
        $this->description="";
        $this->body="";
    }
    public function render()
    {
        return view('livewire.create-article');
    }
}
