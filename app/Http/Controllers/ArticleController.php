<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function createArticle(){
        return view('articles.create');
    }

    public function indexArticle(){
        $articles = Article::take(6)->get()->sortByDesc('created_at');
        return view('articles.index', compact('articles'));
    }
}
