<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    public function index(){
        return view('activity.index');
    }

    public function aScuolaDiInfinito(Category $category){
        return view('activity.scuola-infinito', compact('category'));
    }

    public function giocandoImpara(){
        return view('activity.giocando-impara');
    }

    public function danzaMeditativa(){
        return view('activity.danza-meditativa');
    }

    public function danzaMeditativaChakra(){
        return view('activity.danza-meditativa-chakra');
    }

    public function campoAperto(){
        return view('activity.campo-aperto');
    }

    public function mandala(){
        return view('activity.mandala');
    }

    public function suonoDellaVita(){
        return view('activity.suono-della-vita');
    }
}
