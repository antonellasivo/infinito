<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function createEvent(){
        return view('events.create');
    }

    public function indexEvent(){
        $events = Event::take(6)->get()->sortByDesc('created_at');
        return view('events.index', compact('events'));
    }
}
