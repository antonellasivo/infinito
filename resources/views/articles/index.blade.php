<x-layout>
    <div class="container-fluid my-5">
        <div class="row">
          @foreach ($articles as $article)
            <div class="col-12 col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="https://picsum.photos/200" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">{{$article->title}}</h5>
                      <p class="card-text">{{$article->description}}</p>
                    {{-- <p class="card-footer"> Pubblicato il: {{$article->created_at->format('d/m/Y')}}</p> --}}
                      <a href="#" class="btn btn-primary">Leggi</a>
                    </div>
                  </div>
               </div>
            @endforeach
        </div>
    </div>

</x-layout>