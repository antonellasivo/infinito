<x-layout>
    <x-slot name="title">Crea un evento</x-slot>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <livewire:create-event/>
            </div>
        </div>
    </div>
</x-layout>