<x-layout>
    <div class="container-fluid">
        <h2 class="text-center text-light py-4">Tutti gli eventi in programma</h2>
        <div class="row">
            @foreach ($events as $event)
            <div class="col-12 col-md-4 py-3">
                <div class="card" style="width: 18rem;">
                    <img src="https://picsum.photos/200" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">{{$event->title}}</h5>
                      <p class="card-text">{{$event->description}}</p>
                      <a href="{{$event->category->route}}" class="my-2 border-top pt-2 border-dark card-link shadow btn btn-success">
                        Attività: {{$event->category->name}}
                    </a>
                    <p class="card-footer"> Pubblicato il: {{$event->created_at->format('d/m/Y')}}</p>
                      <a href="#" class="btn btn-primary">Leggi</a>
                    </div>
                </div>    
            </div>
            @endforeach
        </div>
    </div>

</x-layout>