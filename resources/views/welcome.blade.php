<x-layout-home>
    <x-slot name="title">Infinito</x-slot>

<div class="container-fluid top-header min-vh-100 py-5">
    <div class="row ">
        <div class="col-12 col-md-6">
            <div class="d-flex justify-content-end">
                <img src="/img/fiore.png" alt="">

            </div>
        </div>
        <div class="col-12 col-md-6 d-flex align-items-center">
            <div class="justify-content-start">
                <h1 class="text-center my-3 text-light ">
                    INFINITO
                </h1> 
                <h4 class="text-center my-3 text-light ">
                    Centro attivo di risorse educative
                </h4>

            </div>
        </div>
    </div>
    <section>
        <div class="container my-5">
            <h2 class="text-center my-5">Gli ultimi articoli</h2>
            <div class="row ">
                @foreach ($articles as $article)
                    <div class="col-12 col-md-6 col-lg-4 my-2 d-flex justify-content-center">
                        <div class="card" style="width: 18rem;">
                            <img src="https://picsum.photos/200" class="card-img-top" alt="...">
                            <div class="card-body">
                              <h5 class="card-title">{{$article->title}}</h5>
                              <p class="card-text">{{$article->description}}</p>
                            <p class="card-footer"> Pubblicato il: {{$article->created_at->format('d/m/Y')}}</p>
                              <a href="#" class="btn btn-primary">Leggi</a>
                            </div>
                          </div>
                       </div>
                    @endforeach
            </div>
        </div>
    </section>
</div>
<button id='boldButton'>Grassetto</button>
<input type="text">

</x-layout-home>