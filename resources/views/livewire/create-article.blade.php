<div>
   <h1>Crea il tuo articolo</h1>

   @if(session()->has('message'))
        <div class="flex flex-row justify-center my-2 alert alert-success">
            {{session('message')}}
        </div>
   @endif

<form wire:submit.prevent="store">
    @csrf

    <div class="mb-3">
        <label for="title">
            Titolo
        </label>
        <input type="text" wire:model="title" class="form-control @error('title') is-invalid @enderror">
        @error('title')
            {{$message}}
        @enderror
    </div>

    <div class="mb-3">
        <label for="description">
            Descrizione
        </label>
        <input type="text" wire:model="description" class="form-control @error('description') is-invalid @enderror">
        @error('description')
            {{$message}}
        @enderror
    </div>

    <div class="mb-3">
        <label for="body">
            Testo
        </label>
        <textarea type="text" wire:model="body" rows="10" cols="50" class="form-control @error('body') is-invalid @enderror">
            
       </textarea>
        @error('body')
            {{$message}}
        @enderror
    </div>

    <button type="submit" class="btn btn-primary shadow px-4 py-2">
        Crea
    </button>
</form>


</div>
