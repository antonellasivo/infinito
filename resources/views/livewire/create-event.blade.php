<div>
    <h1>Crea un nuovo evento</h1>

   @if(session()->has('message'))
        <div class="flex flex-row justify-center my-2 alert alert-success">
            {{session('message')}}
        </div>
   @endif

<form wire:submit.prevent="store">
    @csrf

    <div class="mb-3">
        <label for="title">
            Titolo dell'evento
        </label>
        <input type="text" wire:model="title" class="form-control @error('title') is-invalid @enderror">
        @error('title')
            {{$message}}
        @enderror
    </div>

    <div class="mb-3">
        <label for="description">
            Descrizione dell'evento
        </label>
        <textarea type="text" wire:model="description" class="form-control @error('description') is-invalid @enderror"></textarea>
        @error('description')
            {{$message}}
        @enderror
    </div>

    <div class="mb-3">
       <h6>Data dell'evento</h6>
       <div class="row">
           <div class="col-12 col-md-4">
                <label for="day">
                    Giorno
                </label>
                <input type="number" wire:model="day" class="form-control @error('day') is-invalid @enderror">
                @error('day')
                    {{$message}}
                @enderror
           </div>
           <div class="col-12 col-md-4">
                <label for="month">
                    Mese
                </label>
                <input type="number" wire:model="month" class="form-control @error('month') is-invalid @enderror">
                @error('month')
                    {{$message}}
                @enderror
            </div>
            <div class="col-12 col-md-4">
                <label for="year">
                    Anno
                </label>
                <input type="number" wire:model="year" class="form-control @error('year') is-invalid @enderror">
                @error('year')
                    {{$message}}
                @enderror
            </div>
       </div> 
    </div>
    <div class="mb-3">
        <label for="category">Attività</label>
        <select wire:model.defer="category" id="category" class="form-control">
            <option value="">Scegli l'attività</option>
            @foreach ($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>
    </div>


    <button type="submit" class="btn btn-primary shadow px-4 py-2">
        Crea
    </button>
</form>


</div>

</div>
