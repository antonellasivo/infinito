<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title ?? ''}}</title>
    {{-- LIVEWIRE --}}
    @livewireStyles
    {{-- CSS --}}
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
    
    <x-navbar/>
    
    <div class="min-vh-100">
        {{$slot}}

    </div>


    @livewireScripts
    <script src="{{asset('js/app.js')}}"></script>
</body>
</html>