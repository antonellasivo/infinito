<x-layout>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h1>Registrati</h1>
                    <form method="POST" action="{{route('register')}}">
                        @csrf
                        <div class="mb-3">
                            <label  class="form-label">Inserisci il tuo nome</label>
                            <input type="text" class="form-control" name="name">
                        </div>
                        <div class="mb-3">
                            <label  class="form-label">Inserisci la tua mail</label>
                            <input type="email" class="form-control" name="email">
                        </div>
                        <div class="mb-3">
                            <label  class="form-label">Password</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <div class="mb-3">
                            <label  class="form-label">Conferma la tua password</label>
                            <input type="password" class="form-control"name="password_confirmation">
                        </div>
                        
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                
            </div>
        </div>
    </div>


</x-layout>