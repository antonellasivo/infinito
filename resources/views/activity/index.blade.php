<x-layout>
    <h2 class="text-center text-light py-5">Le nostre attività e corsi</h2>


    <div class="container-fluid">
        <div class="row justify-content-around">
            <div class="col-12 col-md-5 col-lg-4">
                <a href="{{route('aScuolaDiInfinito')}}" class="text-decoration-none">
                    <div class="card mb-3 activity-card" ">
                        <div class="row g-0 align-items-center">
                            <div class="col-md-4 ">
                                <img src="/img/1.jpg" class="img-fluid rounded-start mtop-15 px-1" alt="...">
                            </div>
                            <div class="col-md-7">
                                <div class="card-body">
                                    <h5 class="card-title">A scuola d'Infinito</h5>
                                    <p class="card-text">Formazione teorico pratica per conseguire e trasmettere l’abilità al gioco e alla gioia.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-5 col-lg-4">
                <a href="{{route('giocandoImpara')}}" class="text-decoration-none">
                    <div class="card mb-3 activity-card" ">
                        <div class="row g-0 align-items-center">
                            <div class="col-md-4">
                                <img src="/img/2.jpg" class="img-fluid rounded-start  mtop-15 px-1" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">Giocando s'impara</h5>
                                    <p class="card-text">Per educare i bambini alla relazione con se stessi e col gruppo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-5 col-lg-4">
                <a href="{{route('danza-meditativa')}}" class="text-decoration-none">
                    <div class="card mb-3 activity-card" ">
                        <div class="row g-0 align-items-center">
                            <div class="col-md-4">
                                <img src="/img/3.jpg" class="img-fluid rounded-start  mtop-15 px-1" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">A scuola di Danza Meditativa</h5>
                                    <p class="card-text">
                                        Formazione teorico-pratica per acquisire e diffondere l’attitudine a danzare con la Vita</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-5 col-lg-4">
                <a href="{{route('danza-meditativa-chakra')}}" class="text-decoration-none">
                    <div class="card mb-3 activity-card" ">
                        <div class="row g-0 align-items-center">
                            <div class="col-md-4">
                                <img src="/img/4.jpg" class="img-fluid rounded-start  mtop-15 px-1" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">Danza Meditativa con i Chakra</h5>
                                    <p class="card-text">Un incontro al mese d’intenso movimento meditativo</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-5 col-lg-4">
                <a href="{{route('campo-aperto')}}" class="text-decoration-none">
                    <div class="card mb-3 activity-card" ">
                        <div class="row g-0 align-items-center">
                            <div class="col-md-4">
                                <img src="/img/5.jpg" class="img-fluid rounded-start  mtop-15 px-1" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">Lezioni in campo aperto</h5>
                                    <p class="card-text">Educare a un contatto diretto con la natura vuol dire seminare conoscenza e rispetto. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-5 col-lg-4">
                <a href="{{route('mandala')}}" class="text-decoration-none">
                    <div class="card mb-3 activity-card" ">
                        <div class="row g-0 align-items-center">
                            <div class="col-md-4">
                                <img src="/img/6.jpg" class="img-fluid rounded-start  mtop-15 px-1" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">Mandala: Cerchi di Sé</h5>
                                    <p class="card-text">Un corso che ti portera’ alla scoperta dei mandala ti fornira’ strumenti utili per comprendere meglio te stesso e gli altri attraverso la creativita’, l’apprendimento e la condivisione.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-5 col-lg-4">
                <a href="{{route('suono-della-vita')}}" class="text-decoration-none">
                    <div class="card mb-3 activity-card" ">
                        <div class="row g-0 align-items-center">
                            <div class="col-md-4">
                                <img src="/img/7.jpg" class="img-fluid rounded-start  mtop-15 px-1" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">Il Suono della Vita</h5>
                                    <p class="card-text">La musica e il canto dell'Infinito sono ovunque e arrivano fino a noi.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</x-layout>