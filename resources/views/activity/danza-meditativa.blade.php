<x-layout>

    <header>
        <h2 class="text-center"> A scuola di Danza Meditativa</h2>
    </header>
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-2">
                    <a href="{{route('activity.index')}}" class="btn btn-success">
                        Torna indietro
                    </a>
            </div>
        </div>
    </div>
    

    <div class="container">
        <div class="row">
            <div class="col-12">
                <button id="boldButton">Ciao</button>
            </div>
        </div>
    </div>
    
    </x-layout>