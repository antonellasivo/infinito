<x-layout>

    <header>
        <h2 class="text-center"> Mandala:Cerchi di Sé</h2>
    </header>
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-2">
                    <a href="{{route('activity.index')}}" class="btn btn-success">
                        Torna indietro
                    </a>
            </div>
        </div>
    </div>
    
    
    </x-layout>