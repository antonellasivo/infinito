<x-layout>

<header>
    <h2 class="text-center"> Lezioni in campo aperto</h2>
</header>

<div class="container-fluid">
    <div class="row">
        <div class="col-22 col-md-2">
                <a href="{{route('activity.index')}}" class="btn btn-success">
                    Torna indietro
                </a>
        </div>
    </div>
</div>


</x-layout>